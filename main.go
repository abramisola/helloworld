package main

import (
  "fmt"
  "log"
  "net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
  if name := r.FormValue("name"); name != "" {
      fmt.Fprintf(w, "Hello, %s!", name)
      return
  }

  fmt.Fprint(w, "Hello, world!")
}

func main() {
  http.HandleFunc("/", handler)

  if err := http.ListenAndServe(":5000", nil); err != nil {
    log.Fatalf("failed to listen on 5000")
  }
}
