package main

import (
  "fmt"
  "io/ioutil"
  "net/http"
  "net/http/httptest"
  "testing"
)

func testRequest(t *testing.T, name, expected string) {
  t.Helper()

  url := "http://example.com"
  if name != "" {
    url = fmt.Sprintf("%s?name=%s", url, name)
  }

  req := httptest.NewRequest("GET", url, nil)
	w := httptest.NewRecorder()

  handler(w, req)

  resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body) // Cannot fail the read here?

  if resp.StatusCode != http.StatusOK {
    t.Errorf("bad status code: expected %d, got %d", http.StatusOK, resp.StatusCode)
  }

  if body := string(body); body != expected {
    t.Errorf("bad response body: expected %+v, got %+v", expected, body)
  }
}

func TestHandler(t *testing.T) {
  tests := []struct{
    name string
    expected string
  }{
    {"", "Hello, world!"},
    {"Abram", "Hello, Abram!"},
    {"Lexi", "Hello, Lexi!"},
    {"Charlie", "Hello, Charlie!"},
    {"Ajla", "Hello, Ajla!"},
  }

  for _, tt := range tests {
    testRequest(t, tt.name, tt.expected)
  }
}
